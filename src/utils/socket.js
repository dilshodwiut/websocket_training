import { store } from "../redux/store";
import {
  notifAction,
  messAction,
  countAction,
  showActionBar,
} from "../redux/actions";

function connectSocket() {
  const ws = new WebSocket("wss://ws.car24.uz/ws-random-2");

  ws.onopen = function () {
    console.log("Connected to the websocket");
    ws.send("we have been connected to the web-socket :)");
    store.dispatch(showActionBar(undefined));
  };

  ws.onmessage = function (mes) {
    try {
      const data = JSON.parse(mes.data);

      if (data.type === "notification") {
        store.dispatch(notifAction(data.data.description));
      } else if (data.type === "message") {
        store.dispatch(messAction(data.data.description));
      } else if (data.type === "progress") {
        store.dispatch(countAction(data.data.count));
      }
    } catch (err) {
      console.log(err);
    }
  };

  ws.onclose = function (e) {
    console.log(`Socket is closed.`, e.reason);
    store.dispatch(showActionBar(connectSocket));
  };

  ws.onerror = function (err) {
    console.error("Socket encountered error: ", err.message, "Closing socket");
    ws.close();
  };
  return ws;
}

export { connectSocket };
