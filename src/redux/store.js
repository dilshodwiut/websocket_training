import { createStore } from "redux";
import wsReducer from "./reducers";

export const store = createStore(wsReducer);
