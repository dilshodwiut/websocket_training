import {
  ADD_NOTIFICATION,
  ADD_MESSAGE,
  UPDATE_COUNT,
  SHOW_ACTION_BAR,
} from "../constants";

export default function wsReducer(
  state = { notification: "", message: "", count: 0, reload: undefined },
  action
) {
  const ap = action.payload;
  switch (action.type) {
    case ADD_NOTIFICATION:
      return { ...state, notification: ap.notification };
    case ADD_MESSAGE:
      return { ...state, message: ap.message };
    case UPDATE_COUNT:
      return { ...state, count: ap.count };
    case SHOW_ACTION_BAR:
      return { ...state, reload: ap };
    default:
      return state;
  }
}
