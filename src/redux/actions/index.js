import {
  ADD_NOTIFICATION,
  ADD_MESSAGE,
  UPDATE_COUNT,
  SHOW_ACTION_BAR,
} from "../constants";

export const notifAction = (notif) => {
  return { type: ADD_NOTIFICATION, payload: { notification: notif } };
};

export const messAction = (mess) => {
  return { type: ADD_MESSAGE, payload: { message: mess } };
};

export const countAction = (count) => {
  return { type: UPDATE_COUNT, payload: { count: count } };
};

export const showActionBar = (cb) => {
  return { type: SHOW_ACTION_BAR, payload: cb };
};
