import { useState } from "react";
import "./App.css";
import SiderDemo from "./pages/main";
import { useEffect } from "react";
import { connectSocket } from "./utils/socket";
import { useSelector } from "react-redux";

function App() {
  const reload = useSelector((state) => state.reload);
  console.log(reload);

  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    connectSocket();
  }, []);

  useEffect(() => {
    if (typeof reload === "function") {
      setHasError(true);
    } else {
      setHasError(false);
    }
  }, [reload]);

  return (
    <div className="App">
      <SiderDemo hasError={hasError} reconnectHandler={reload} />
    </div>
  );
}

export default App;
