import React from "react";
import { Layout, Menu } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import MainContent from "./content";
import { connect } from "react-redux";
import { showActionBar } from "../redux/actions";
import CloudOffIcon from "@material-ui/icons/CloudOff";

const { Header, Sider, Content } = Layout;

class SiderDemo extends React.Component {
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    return (
      <>
        {this.props.hasError && (
          <div
            onClick={this.props.reconnectHandler}
            style={{
              backgroundColor: "red",
              color: "white",
              textAlign: "center",
              padding: "1rem",
              fontSize: "1.2rem",
              fontWeight: "500",
              lineHeight: "1.5",
            }}
          >
            <CloudOffIcon
              fontSize="large"
              className=""
              style={{ verticalAlign: "bottom", margin: "0 1rem" }}
            />
            Disconnected from the Internet. Please, click to retry
          </div>
        )}
        <Layout>
          <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
            <div className="logo" />
            <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
              <Menu.Item key="1" icon={<UserOutlined />}>
                nav 1
              </Menu.Item>
              <Menu.Item key="2" icon={<VideoCameraOutlined />}>
                nav 2
              </Menu.Item>
              <Menu.Item key="3" icon={<UploadOutlined />}>
                nav 3
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout className="site-layout">
            <Header className="site-layout-background" style={{ padding: 0 }}>
              {React.createElement(
                this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: this.toggle,
                }
              )}
            </Header>
            <Content
              className="site-layout-background"
              style={{
                margin: "24px 16px",
                padding: 24,
                minHeight: 280,
              }}
            >
              <MainContent />
            </Content>
          </Layout>
        </Layout>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  reload: state.reload,
});

const mapDispatchToProps = () => ({
  showActionBar,
});

export default connect(mapStateToProps, mapDispatchToProps())(SiderDemo);
