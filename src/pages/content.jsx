import { useState, useEffect } from "react";
import { Progress } from "antd";
import { useSelector } from "react-redux";
import { Button, notification } from "antd";
import { DownloadOutlined } from "@ant-design/icons";

export default function Content() {
  const progress = useSelector((state) => state.count);
  const message = useSelector((state) => state.message);
  const notif = useSelector((state) => state.notification);

  const [prog, setProg] = useState(false);

  useEffect(() => {
    if (prog) {
      notification.open({
        key: "progress",
        message: "Downloading, please wait...",
        description: <Progress percent={progress} />,
        duration: 0,
        onClose: () => setProg(false),
      });
    }
  }, [prog, progress]);

  useEffect(() => {
    notif &&
      notification.info({
        key: "notification",
        message: notif,
        description: message,
        duration: 5,
        placement: "topLeft",
      });
  }, [notif, message]);

  return (
    <>
      <div className="content">
        <Button
          type="primary"
          shape="round"
          icon={<DownloadOutlined />}
          size={"large"}
          onClick={() => setProg(true)}
        >
          Download
        </Button>
      </div>
    </>
  );
}
